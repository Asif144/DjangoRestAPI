
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework import filters
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from  rest_framework.permissions import IsAuthenticated


from . import serializers
from . import models
from . import permission
# Create your views here.

class HelloApiView(APIView):

    serializer_class = serializers.HelloSerializer

    def get(self, request, format=None):

        an_apiview = [
        'Uses HTTP methods as function(get, post,patch,delete,put)',
        'It is similar to a traditional Django view',
        ]
        return Response({'message':'hello','an_apiview':an_apiview})


    def post(self, request):
        """"""
        serializer = serializers.HelloSerializer(data=request.data)

        if serializer.is_valid():
            name = serializer.data.get('name')
            message = 'hello {0}'.format(name)
            return Response({'message': message})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, pk=None):
        """ updating an object """
        return Response({'method':'put'})

    def patch(self, request, pk=None):
        return Response({'method':'patch'})

    def delete(self, request, pk=None):
        """ delete an object """
        return Response({'method': 'delete'})



class HelloViewSet(viewsets.ViewSet):
    """API viewSet"""
    serializer_class = serializers.HelloSerializer
    def list(self, request):
        """ Return a hello message """
        a_viewset = [
        'autometically maps to urls using routers'
        ]

        return Response({'message':'hello!!!!', 'a_viewset': a_viewset})

    def create(self, request):
        """ create a new hello message """
        serializer = serializers.HelloSerializer(data=request.data)
        if serializer.is_valid():
            name = serializer.data.get('name')
            message = 'Hello {0}'.format(name)
            return Response({'message': message})
        else:
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    def retrive(self, request, pk=None):
        """ handles getting object by its ID """
        return Response({'http_method': 'GET'})

    def update(self, request, pk=None):
        """ handles updating an object """
        return Response({'http_method': 'PUT'})

    def partial_update(self, request, pk=None):
        """ handles updating an object """
        return Response({'http_method': 'PATCH'})

    def destroy(self, request, pk=None):
        """ handles updating an object """
        return Response({'http_method': 'DELETE'})



class UserProfileViewSet(viewsets.ModelViewSet):
    """ create, delete , update profile """

    serializer_class = serializers.UserProfileSerializer
    queryset = models.UserProfile.objects.all()
    authentication_classes = (TokenAuthentication, )
    permission_classes = (permission.UpdateOwnProfile,)
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', 'email', )


class LoginViewSet(viewsets.ViewSet):
    """ check email and password and returns an auth token """
    serializer_class = AuthTokenSerializer

    def create(self, request):
        """ use the ObtainAuthToken APIView to validate and create a token"""
        return ObtainAuthToken().post(request)

class UserProfileFeedViewSet(viewsets.ModelViewSet):
    """ handles creating, reading and updating profile feed item """
    authentication_classes = (TokenAuthentication,)
    serializer_class = serializers.ProfileFeedItemSerializer
    queryset = models.ProfileFeedItem.objects.all()
    permission_classes = (permission.PostOwnStatus, IsAuthenticated)
    def perform_create(self, serializer):
        """ sets the user profile to logged in user """

        serializer.save(user_profile=self.request.user)