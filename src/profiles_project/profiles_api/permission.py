from rest_framework import permissions

class UpdateOwnProfile(permissions.BasePermission):
    """ allow users to edit thei own data """

    def has_object_permission(self, request, view, obj):
        """ check user is trying their own profile """

        if request.method in permissions.SAFE_METHODS:
            return True  # if this is flase it means user trying to update or delete
        return obj.id == request.user.id # it returns true if user try to update or delete his own data




class PostOwnStatus(permissions.BasePermission):
    """ allow users to edit thei own data """

    def has_object_permission(self, request, view, obj):
        """ check user is trying their own profile """

        if request.method in permissions.SAFE_METHODS:
            return True  # if this is flase it means user trying to update or delete
        return obj.user_profile.id == request.user.id # it returns true if user try to update or delete his own data
