from django.conf.urls import url
from django.conf.urls import include
from rest_framework.routers import DefaultRouter
from . import views


router = DefaultRouter()
router.register('hello-viewset', views.HelloViewSet, base_name = 'hello-viewset')
'''
base_name is used in this HelloViewSet bcoz the viewSet is not a ModelViewSet

'''
router.register('profile', views.UserProfileViewSet)
router.register('login', views.LoginViewSet, base_name = 'login')
router.register('feed', views.UserProfileFeedViewSet)

urlpatterns = [
    url(r'^hello_view/', views.HelloApiView.as_view()),
    url(r'',include(router.urls)),
]
