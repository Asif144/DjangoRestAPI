#Project REST API Django

REST API providing basic functionality for managing user profiles.


##Create / Run / Start  vagrant :
1. vagrant up
##Start virtual server:
1. vagrant ssh

##Go to vagrant directory in virtual server and watches the files:
1. cd / vagrant
2. ls
3. ls -a

##Creat virtual env for python
1. mkvirtualenv profiles_rest_api --python=python3

##Enter newly created virtual env
1.  workon profiles_rest_api
###Install python packages in this virtual env
1. pip install django==1.11
2. pip install djangorestframework==3.6.2

##Create Django Project with rest framework
###Create a folder name src in the project directory
1.  cd /vagrant/src
2. django-admin.py startproject profiles_project
#####create new app into the Project
1. cd profiles_project
2. python manage.py startapp profiles_api
3. python manage.py runserver 0.0.0.0:8080
   ####0.0.0.0 ensures for all ip address
4. goto this link -> http://127.0.0.1:8080/  
##Add requrement txt
1. goto vagrant directory
2. pip freeze > requrement.txt
